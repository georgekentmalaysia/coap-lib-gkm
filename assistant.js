var crc16 = require("./crc16");
var aesjs = require('aes-js');
var bitconverter = require("./bitconverter");
var exports = module.exports={};

exports.compareChecksum = function (payload) {
    let result = false;

    var rxByteChecksum = new Buffer([payload[payload.length - 1], payload[payload.length - 2]]);
    var payloadChecksum = Buffer.alloc(payload.length - 2);
    payload.copy(payloadChecksum, 0, 0, payload.length - 2);

    var myChecksum = calculateChecksum(payloadChecksum);

    if (compareChecksum(myChecksum, rxByteChecksum)) {
        result = true;
    }

    return result;
};

exports.getPlainPayload = function (payload, decryptKey) {
    return CTRSecureGet(payload, decryptKey);
};

exports.setEncryptedPayload = function (uplinkPacketHeader, plainPayload, encryptKey) {
    return CTRSecureSet(uplinkPacketHeader, plainPayload, encryptKey);
};

exports.toHexString = function (byteArray) {
    return Array.from(byteArray, function (byte) {
        return ('0' + (byte & 0xFF).toString(16)).slice(-2);
    }).join('');
};

var hexToAscii = function (strIn) {
    var hexIn = strIn.toString();
    var strOut = '';
    for (var n = 0; n < hexIn.length; n += 2) {
        strOut += String.fromCharCode(parseInt(hexIn.substr(n, 2), 16));
    }
    return strOut;
};

exports.hexToAscii=hexToAscii;

var changeHexOrder = function (strIn) {
    var arr = strIn.split("");
    var tmp;
    for (var i = 0; i < arr.length / 2; i += 2) {
        tmp = arr[i];
        arr[i] = arr[arr.length - i - 2];
        arr[arr.length - i - 2] = tmp;

        tmp = arr[i + 1];
        arr[i + 1] = arr[arr.length - i - 1];
        arr[arr.length - i - 1] = tmp;
    }

    var t = arr.join("");
    return t;
};

exports.changeHexOrder=changeHexOrder;

exports.getTxPayloadwCheckSum = function (hexResponseStatus, synctime) {
    var txChecksum = calculateChecksumFromString(hexResponseStatus + synctime);
    var txPayload = hexResponseStatus +
        synctime +
        ('0' + (txChecksum[1] & 0xFF).toString(16)).slice(-2) +
        ('0' + (txChecksum[0] & 0xFF).toString(16)).slice(-2);
    return txPayload;
};

exports.toHex = function (byte) {
    return ('0' + (byte & 0xFF).toString(16)).slice(-2);
};

exports.getSyncTime = function () {
    var d = new Date(),
        seconds = '' + d.getSeconds(),
        minutes = '' + d.getMinutes(),
        hours = '' + d.getHours(),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear().toString().substring(2, 4);

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;
    if (hours.length < 2)
        hours = '0' + hours;
    if (minutes.length < 2)
        minutes = '0' + minutes;
    if (seconds.length < 2)
        seconds = '0' + seconds;

    return [hours, minutes, seconds, day, month, year].join('');
};

function calculateChecksum(payload){
    var checksum = crc16.computeChecksum(payload);
    let b = Buffer.alloc(2);
    b.writeUInt16LE(checksum, 0);
    return b;
}

function calculateChecksumFromString(hexstring){
    var payload = hexToBytes(hexstring);
    var checksum = crc16.computeChecksum(payload);
    let b = Buffer.alloc(2);
    b.writeUInt16LE(checksum, 0);
    return b;
}

function compareChecksum(calCheckSum, receivedCheckSum)
{
    if (calCheckSum.Length !== receivedCheckSum.Length ||
        calCheckSum[0] !== receivedCheckSum[0] ||
        calCheckSum[1] !== receivedCheckSum[1])
        return false;
    else
        return true;
}

function CTRSecureGet(uplinkPacket, key)
{   
    var iv = Buffer.alloc(16);
    var encryptedPayload = Buffer.alloc(uplinkPacket.length - (15 + 1 + 4 + 1 + 2));
    var plainPayload = Buffer.alloc(encryptedPayload.length);
    var outputPacket = Buffer.alloc(uplinkPacket.length - (4 + 2));

    /*construct IV*/
    uplinkPacket.copy(iv, 0, 2, 14);
    uplinkPacket.copy(iv, 12, 16, 20);

    /*copy encryptedPayload*/
    uplinkPacket.copy(encryptedPayload, 0, 20, encryptedPayload.length + 20);

    // create AES cipher
    var aecCtr = new aesjs.ModeOfOperation.ctr(key, iv);
    plainPayload = aecCtr.decrypt(encryptedPayload);
    var checksum = plainPayload[plainPayload.length - 2] << 8 | plainPayload[plainPayload.length - 1];/*there is crc embedded in plain payload to check for authenticity*/
    const checkPlain = new Buffer(plainPayload.slice(0, plainPayload.length - 2));/*remove 2 bytes payload crc at the back*/

    if (0x0000 !== (checksum ^ crc16.computeChecksum(checkPlain)))
    {
        return null;/*checksum not valid*/
    }

    // construct output packet
    uplinkPacket.copy(outputPacket, 0, 0, 16);
    checkPlain.copy(outputPacket, 16, 0, checkPlain.length);
    uplinkPacket.copy(outputPacket, 16 + checkPlain.length, 20 + encryptedPayload.length, 1 + 20 + encryptedPayload.length);/*copy counter*/
    checksum = crc16.computeChecksumWithLength(outputPacket, encryptedPayload.length - 2);
    outputPacket[outputPacket.length - 2] = checksum >> 8;
    outputPacket[outputPacket.length - 1] = checksum >> 0;

    return outputPacket;
}

function CTRSecureSet(uplinkPacketHeader, plainPayload, encryptKey)
{  

    if (null === plainPayload || 0 === plainPayload.length)
    {
        return plainPayload;/*no encrypted payload*/
    }

    var iv = Buffer.alloc(16);


    /*construct IV*/
    uplinkPacketHeader.copy(iv, 0, 2, 14);
    uplinkPacketHeader.copy(iv, 12, 16, 20);

    /*append crc to plain payload*/
    var _checksum = crc16.computeChecksum(plainPayload);
    const checkPlain = Buffer.alloc(plainPayload.length + 2);
    plainPayload.copy(checkPlain, 0, 0, plainPayload.length);
    checkPlain[checkPlain.length - 2] = _checksum >> 8;
    checkPlain[checkPlain.length - 1] = _checksum;

    // create AES cipher
    var aecCtr = new aesjs.ModeOfOperation.ctr(encryptKey, iv);

    return aecCtr.encrypt(checkPlain);
}

var hexToBytes = function (strIn) {
    return Buffer.from(strIn, 'hex');
};

exports.hexToBytes=hexToBytes;

var stringToBytes = function (strIn) {
    return Buffer.from(strIn);
};

exports.stringToBytes=stringToBytes;

var bytesToString = function (buf, type) {
    return buf.toString(type);
};

exports.bytesToString=bytesToString;


exports.splitRegisterPacket = function (inputPacket) {
    var data = {};

    data.hardwareVersion = hexToAscii(inputPacket.substring(32, 42));
    data.firmwareVersion = hexToAscii(inputPacket.substring(42, 52));
    data.transmitInterval = parseInt(changeHexOrder(inputPacket.substring(52, 60)), 16);
    data.logInterval = parseInt(changeHexOrder(inputPacket.substring(60, 68)), 16);
    data.longitude = bitconverter.getBytes(parseInt(inputPacket.substring(68, 76), 16)).readFloatBE(0);
    data.latitude = bitconverter.getBytes(parseInt(inputPacket.substring(76, 84), 16)).readFloatBE(0);
    data.meterModel = parseInt(changeHexOrder(inputPacket.substring(84, 86)), 16);
    data.initialMeterReading = bitconverter.getBytes(parseInt(inputPacket.substring(86, 94), 16)).readFloatBE(0);
    data.initialPulseReading = parseInt(changeHexOrder(inputPacket.substring(94, 102)), 16);

    return data;
};

exports.readingTimestamp = function (readingDateTime) {
    var yy = parseInt("20" + readingDateTime.substring(10, 12));
    var MM = parseInt(readingDateTime.substring(8, 10));
    var dd = parseInt(readingDateTime.substring(6, 8));
    var HH = parseInt(readingDateTime.substring(0, 2));
    var mm = parseInt(readingDateTime.substring(2, 4));
    var ss = parseInt(readingDateTime.substring(4, 6));

    return new Date(yy, MM - 1, dd, HH, mm, ss);
};

exports.booleanToBitArray = function (bits, reverse) {
    // Private field - array for our bits
    var m_bits = new Array();

    if (reverse) {
        bits.reverse();
    }

    //.ctor - initialize as a copy of an array of true/false or from a numeric value
    if (bits && bits.length) {
        for (var i = 0; i < bits.length; i++)
            m_bits.push(bits[i] ? 1 : 0);
    }

    return m_bits;
};

