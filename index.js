'use strict';

var exports = module.exports={};
const assistant = require("./assistant");
const bitconverter = require("./bitconverter");
const rs = require("./responsestatus");

exports.getRegisterPacketOutput=function(rxPayload , decryptKey, encryptKey){
    var output = {};
    var count = null;
    var plainPayload = null;
    var ableToDecrypt = false;
    const synctime = assistant.getSyncTime();

    output.startProcess = new Date();
    output.hasError = true;

    if(rxPayload){
        output.miuSerialNumber = sliceBuf(rxPayload,0,15);
        count = rxPayload[rxPayload.length - 3];
        output.packetType = rxPayload[15];

        if(assistant.compareChecksum(rxPayload)){
            plainPayload = assistant.getPlainPayload(rxPayload, decryptKey);

            if(plainPayload){
                output.firmwareVersion = plainPayload.toString('utf8', 21, 26);
                output.transmitInterval = plainPayload.readUInt32LE(26);
                output.logInterval = plainPayload.readUInt32LE(30);
                
                var plainData = assistant.toHexString(plainPayload);
                output.data = assistant.splitRegisterPacket(plainData);
                output.data.miuSerialNumber = output.miuSerialNumber;

                output.hasError = false;
                ableToDecrypt = true;

            }else{
                output.hasError = true;
                output.errorMessage = "Unable to decrypt payload.";
            }

            var respPayload = assistant.setEncryptedPayload(rxPayload, Buffer.from(synctime + "00", "hex"), encryptKey);
            var respPayloadBuffer = Buffer.from(respPayload);
            var respPayloadString = respPayloadBuffer.toString('hex');

            if(ableToDecrypt){
                output.txPayload = assistant.getTxPayloadwCheckSum(assistant.toHex(rs.getCode("Succeed")), respPayloadString + assistant.toHex(count));
            }else{
                output.txPayload = assistant.getTxPayloadwCheckSum(assistant.toHex(rs.getCode("FatalError")), respPayloadString + assistant.toHex(count));
            }

        }else{
            output.hasError = true;
            output.errorMessage = "Invalid checksum.";
            output.txPayload = assistant.getTxPayloadwCheckSum(assistant.toHex(rs.getCode("InvalidCheckSum")) + "FF");
        }
    }else{
        output.hasError = true;
        output.message = "Payload is empty";
    }

    output.endProcess = new Date();

    return output;
};

exports.getMeterDataPacketOutput =function(rxPayload , decryptKey, encryptKey, downlinkCommand){
    var listOfReadings = [];
    var output = {};
    var count = null;
    var plainPayload = null;
    var ableToDecrypt = false;
    const synctime = assistant.getSyncTime();
    var respPayloadString = "";

    output.startProcess = new Date();
    output.hasError = true;

    if(rxPayload){
        output.miuSerialNumber = sliceBuf(rxPayload,0,15);
        count = rxPayload[rxPayload.length - 3];
        output.packetType = rxPayload[15];

        if(assistant.compareChecksum(rxPayload)){
            plainPayload = assistant.getPlainPayload(rxPayload, decryptKey);

            if(plainPayload){
                
                var plainData = assistant.toHexString(plainPayload);
                console.log(plainData);

                if(plainData.length < 37){

                    output.hasError = true;
                    output.errorMessage = "Data corrupted.";
                    
                }else{
                    var meter = {};
                    meter.miuSerialNumber = assistant.hexToAscii(plainData.substring(0,30) + "00");
                    meter.packetType = parseInt(assistant.stringToBytes(plainData.substring(30, 32)),16);
                    meter.packetVersion = meter.packetType & 0xF0;
                    meter.rssi = parseInt(plainData.substring(32,34), 16);

                    var meterPayload = plainData.substring(34, plainData.length);
                    var subPayload = meterPayload.substring(0,32);

                    var reading = {};
                    meter.readings = [];
                    reading.readingDateTime = assistant.readingTimestamp(subPayload.substring(0,12));
                    reading.reading  = parseInt(assistant.changeHexOrder(subPayload.substring(12,20)), 16);
                    reading.temperature = parseInt(subPayload.substring(22,24), 16);
                    meter.readings.push(reading); //first reading

                    meter.battery = assistant.hexToBytes(subPayload.substring(24,32)).readFloatLE(0);
                    meter.alarmStatus = Buffer.from(subPayload.substring(20,22), "hex");

                    var currentbyte = 32;
                    var hasmore1 = parseInt(meterPayload.substring(currentbyte, currentbyte + 2), 16);
                    currentbyte += 2;
                    var offset = currentbyte + hasmore1 * 20;

                    if (meterPayload.length - 4 > offset)
                    {
                        var hasmore2 = parseInt(meterPayload.substring(offset, offset + 2));
                        offset += 2;
                        for (var j = 0; j < hasmore2; j++)
                        {
                            //readingDateTime = meterPayload.substring(offset, offset + 12);                                               
                            offset += 12;
                            meter.npsmr = parseInt(assistant.stringToBytes(meterPayload.substring(offset, offset + 2)));                        
                            offset += 2;
                            meter.cergestatus = parseInt(assistant.stringToBytes(meterPayload.substring(offset, offset + 2)));                  
                            offset += 2;
                            meter.tac = assistant.changeHexOrder(meterPayload.substring(offset, offset + 4));                                     
                            offset += 4;
                            meter.rejectcause = parseInt(meterPayload.substring(offset, offset + 2),16);                               
                            offset += 2;
                            meter.activetime = parseInt(meterPayload.substring(offset, offset + 2));                                   
                            offset += 2;
                            meter.periodictau = parseInt(meterPayload.substring(offset, offset + 2));                                  
                            offset += 2;
                            meter.signalpower = parseInt(assistant.changeHexOrder(meterPayload.substring(offset, offset + 4)),16);          
                            offset += 4;
                            meter.totalpower = parseInt(assistant.changeHexOrder(meterPayload.substring(offset, offset +4)),16);           
                            offset += 4;
                            meter.txpower = parseInt(assistant.changeHexOrder(meterPayload.substring(offset, offset + 4)),16);             
                            offset += 4;
                            meter.txtime = parseInt(assistant.changeHexOrder(meterPayload.substring(offset, offset + 8)),16);             
                            offset += 8;
                            meter.rxtime = parseInt(assistant.changeHexOrder(meterPayload.substring(offset, offset + 8)),16);             
                            offset += 8;
                            meter.cellid = assistant.changeHexOrder(meterPayload.substring(offset, offset + 8));                                  
                            offset += 8;
                            meter.ecl = parseInt(meterPayload.substring(offset, offset + 2));                                           
                            offset += 2;
                            meter.snr = parseInt(assistant.changeHexOrder(meterPayload.substring(offset, offset + 4)),16);                 
                            offset += 4;
                            meter.earfcn = parseInt(assistant.changeHexOrder(meterPayload.substring(offset, offset + 4)),16);              
                            offset += 4;
                            meter.pci = parseInt(assistant.changeHexOrder(meterPayload.substring(offset, offset + 4)),16);                 
                            offset += 4;
                            meter.rsrq = parseInt(assistant.changeHexOrder(meterPayload.substring(offset, offset + 4)),16);                
                            offset += 4;
                            meter.selfresetcounter = parseInt(assistant.changeHexOrder(meterPayload.substring(offset, offset + 4)), 16); 
                            offset += 4;
                            meter.hardfaultcounter = parseInt(assistant.changeHexOrder(meterPayload.substring(offset, offset + 4)), 16); 
                            offset += 4;
                            meter.logIndex = parseInt(assistant.changeHexOrder(meterPayload.substring(offset, offset + 4)), 16); 
                            offset += 8;
                            meter.attemptTransmitCounter = parseInt(assistant.changeHexOrder(meterPayload.substring(offset, offset + 4)), 16); 
                            offset += 4;
                            meter.failTransmitCounter = parseInt(assistant.changeHexOrder(meterPayload.substring(offset, offset + 4)), 16); 
                            offset += 4;
                            meter.attachCounter = parseInt(assistant.changeHexOrder(meterPayload.substring(offset, offset + 4)), 16); 
                            offset += 4;
                            meter.disattachCounter = parseInt(assistant.changeHexOrder(meterPayload.substring(offset, offset + 4)), 16); 
                            offset += 4;
                            meter.vRefDippedCounter = parseInt(assistant.changeHexOrder(meterPayload.substring(offset, offset + 4)), 16); 
                            offset += 4;
                            meter.mcuFailsafeRebootCount = parseInt(assistant.changeHexOrder(meterPayload.substring(offset, offset + 4)), 16); 
                            offset += 4;
                        }


                        for (var jj = 0; jj < hasmore1; jj ++)
                        {       
                            var readingMore = {};

                            if(0x10 === meter.packetVersion)/*date is compress to add one byte temperature*/
                            {
                                var time = meterPayload.substring(currentbyte, currentbyte + 6);
                                currentbyte += 6;
                                var date_n_Temp = parseInt(meterPayload.substring(currentbyte, currentbyte + 6), 16);
                                currentbyte += 6;

                                /*date(5bit) month(4bit) year(7bit) temperature(8bit)*/ 
                                readingMore.temperature = date_n_Temp & 0xFF;
                                var  year = (date_n_Temp >> 8 & 0x7F).toString();
                                var month = parseInt((date_n_Temp >> 15 & 0x0F).toString());
                                var date = (date_n_Temp >> 19 & 0x1F).toString();

                                readingMore.readingDateTime = new Date("20" + year, month - 1, date, time.substring(0, 2), time.substring(2, 4), time.substring(4, 6));
                            }
                            else
                            {
                                readingMore.readingDateTime = assistant.readingTimestamp(meterPayload.substring(currentbyte, currentbyte + 12));
                                currentbyte += 12;
                                
                            }

                            readingMore.reading  = parseInt(assistant.changeHexOrder(meterPayload.substring(currentbyte, currentbyte + 8)), 16);               
                            currentbyte += 8;
                            meter.readings.push(readingMore);
                        }
                    }

                    output.data = meter;

                    output.hasError = false;
                    ableToDecrypt = true;
                }

            }else{
                output.hasError = true;
                output.errorMessage = "Unable to decrypt payload.";
            }

            var msg = synctime + "00";
            if(downlinkCommand){
                msg = synctime + downlinkCommand;
            }
            var respPayload = assistant.setEncryptedPayload(rxPayload, Buffer.from(msg, "hex"), encryptKey);
            var respPayloadBuffer = Buffer.from(respPayload);
            respPayloadString = respPayloadBuffer.toString('hex');

            if(ableToDecrypt){
                output.txPayload = assistant.getTxPayloadwCheckSum(assistant.toHex(rs.getCode("Succeed")), respPayloadString + assistant.toHex(count));
            }else{
                output.txPayload = assistant.getTxPayloadwCheckSum(assistant.toHex(rs.getCode("FatalError")), respPayloadString + assistant.toHex(count));
            }

        }else{
            output.hasError = true;
            output.errorMessage = "Invalid checksum.";
            output.txPayload = assistant.getTxPayloadwCheckSum(assistant.toHex(rs.getCode("InvalidCheckSum")), synctime + "00" + "FF");
        }
    }else{
        output.hasError = true;
        output.message = "Payload is empty";
    }

    output.endProcess = new Date();

    return output;
};

exports.getCommand=function(rxPayload , decryptKey, encryptKey, downlinkCommand){
    var output = {};
    var count = null;
    var plainPayload = null;
    var ableToDecrypt = false;
    const synctime = assistant.getSyncTime();
    //const synctime = "111240090320";

    output.startProcess = new Date();
    output.hasError = true;

    if(!downlinkCommand){
        downlinkCommand = "00";
    }

    if(rxPayload){
        output.miuSerialNumber = sliceBuf(rxPayload,0,15);
        count = rxPayload[rxPayload.length - 3];
        output.packetType = rxPayload[15];
        
        if(assistant.compareChecksum(rxPayload)){
            plainPayload = assistant.getPlainPayload(rxPayload, decryptKey);

            if(plainPayload){

                output.hasError = false;
                ableToDecrypt = true;

            }else{
                output.hasError = true;
                output.errorMessage = "Unable to decrypt payload.";
            }

            var respPayload = assistant.setEncryptedPayload(rxPayload, Buffer.from(synctime + downlinkCommand, "hex"), encryptKey);
            var respPayloadBuffer = Buffer.from(respPayload);
            var respPayloadString = respPayloadBuffer.toString('hex');

            if(ableToDecrypt){
                output.txPayload = assistant.getTxPayloadwCheckSum(assistant.toHex(rs.getCode("Succeed")), respPayloadString + assistant.toHex(count));
            }else{
                output.txPayload = assistant.getTxPayloadwCheckSum(assistant.toHex(rs.getCode("FatalError")), respPayloadString + assistant.toHex(count));
            }

        }else{
            output.hasError = true;
            output.errorMessage = "Invalid checksum.";
            output.txPayload = assistant.getTxPayloadwCheckSum(assistant.toHex(rs.getCode("InvalidCheckSum")) + "FF");
        }
    }else{
        output.hasError = true;
        output.message = "Payload is empty";
    }

    output.endProcess = new Date();

    return output;
};

//create multiple commands to transmit to 1 device
exports.createMultipleDownlinkCommands = function (commands) {
    var commandsInfo = {};
    var warningMessage = [];
    commandsInfo.input = commands; //used as reference to output

    var sb = "";
    if (commands.length > 0) {
        sb += assistant.toHex(commands.length);
        for (var k = 0; k < commands.length; k++) {
            var command = commands[k];
            switch (command.commandCode) {
                case "01":
                    sb += assistant.toHex(command.commandCode);
                    sb += assistant.toHex(0); //command value size (bytes)
                    break;
                case "03": /* Configure Transmit Interval */
                    if (typeof command.transmitInterval === 'number') {
                        sb += assistant.toHex(command.commandCode);
                        sb += assistant.toHex(4); //command value size (bytes)
                        sb += assistant.changeHexOrder(command.transmitInterval.toString(16).padStart(8, '0'));
                    } else {
                        warningMessage.push("Check command value parameter for log interval configuration.");
                    }
                    break;
                case "04": /* Configure Log Interval */
                    if (typeof command.logInterval === 'number') {
                        sb += assistant.toHex(command.commandCode);
                        sb += assistant.toHex(4); //command value size (bytes)
                        sb += assistant.changeHexOrder(command.logInterval.toString(16).padStart(8, '0'));
                    } else {
                        warningMessage.push("Check command value parameter for log interval configuration.");
                    }
                    break;
                case "05":/* Configure Clear Alarm */
                    if (command.clearAlarm) {
                        var message = command.clearAlarm.split('');
                        var integer = parseInt(message.reverse().join(''), 2);

                        sb += assistant.toHex(command.commandCode);
                        sb += assistant.toHex(1); //command value size (bytes)
                        sb += assistant.toHex(integer);
                    } else {
                        warningMessage.push("Check command clear alarm parameter.");
                    }
                    break;
                case "06": /* Configure Alarm Mask */
                    if (command.alarmMask) {
                        var message0 = command.alarmMask.split('');
                        var integer0 = parseInt(message0.join(''), 2);

                        sb += assistant.toHex(command.commandCode);
                        sb += assistant.toHex(1); //command value size (bytes)
                        sb += assistant.toHex(integer0);
                    } else {
                        warningMessage.push("Alarm mask config: Check command alarm mask parameter.");
                    }
                    break;
                case "07": /* Configure Voltage */
                    if (typeof command.lowBatteryThreshold === 'number') {
                        sb += assistant.toHex(command.commandCode);
                        sb += assistant.toHex(4); //command value size (bytes)
                        sb += assistant.bytesToString(bitconverter.doubleToBytes(command.lowBatteryThreshold), 'hex').replace("-", "");
                    } else {
                        warningMessage.push("Check low battery threshold parameter.");
                    }
                    break;
                case "08": /* Configure Pipe Parameter */
                    if (typeof command.burstThreshold === 'number'
                        && typeof command.burstSamplingInterval === 'number'
                        && typeof command.noConsumptionSamplingInterval === 'number'
                        && typeof command.leakageThreshold === 'number'
                        && typeof command.leakageSamplingInterval === 'number'
                        && typeof command.backflowThreshold === 'number') {
                        sb += assistant.toHex(command.commandCode);
                        sb += assistant.toHex(24); //command value size (bytes)
                        sb += assistant.bytesToString(bitconverter.doubleToBytes(command.burstThreshold * 1000 / 3600), 'hex').replace("-", ""); /*burst_threshold*/
                        sb += assistant.bytesToString(bitconverter.getBytes(command.burstSamplingInterval), 'hex').replace("-", ""); /*burst_sampling_interval*/
                        sb += assistant.bytesToString(bitconverter.getBytes(command.noConsumptionSamplingInterval), 'hex').replace("-", ""); /*no_consump_sample_interval*/
                        sb += assistant.bytesToString(bitconverter.doubleToBytes(command.leakageThreshold / 3600), 'hex').replace("-", ""); /*leakage_threshold*/
                        sb += assistant.bytesToString(bitconverter.getBytes(command.leakageSamplingInterval), 'hex').replace("-", ""); /*leakage_sample_interval*/
                        sb += assistant.bytesToString(bitconverter.getBytes(command.backflowThreshold), 'hex').replace("-", ""); /*backflow_threshold*/
                    } else {
                        warningMessage.push("Check pipe parameters (burst threshold, burst sampling interval, no consumption sampling interval, leakage threshold, " +
                            "leakage sampling threshold, backflow threshold).");
                    }
                    break;
                case "09": /* Configure COAP IP Port */
                    if (command.coapIp1 && command.coapPort1 && command.coapIp2 && command.coapPort2) {
                        sb += assistant.toHex(command.commandCode);
                        sb += assistant.toHex(42); //command value size (bytes)
                        sb += assistant.bytesToString(Buffer.from(command.coapIp1.padEnd(16, '\0'), 'ascii'), 'hex').replace("-", "");
                        sb += assistant.bytesToString(Buffer.from(command.coapPort1.padEnd(5, '\0'), 'ascii'), 'hex').replace("-", "");
                        sb += assistant.bytesToString(Buffer.from(command.coapIp2.padEnd(16, '\0'), 'ascii'), 'hex').replace("-", "");
                        sb += assistant.bytesToString(Buffer.from(command.coapPort2.padEnd(5, '\0'), 'ascii'), 'hex').replace("-", "");
                    } else {
                        warningMessage.push("Check coap ip/port parameters.");
                    }
                    break;
                case "10": /* Configure COAP Selection */
                    if (typeof command.currentIpOrPort === 'number' && command.currentIpOrPort > 0 && command.currentIpOrPort < 3) {
                        sb += assistant.toHex(command.commandCode);
                        sb += assistant.toHex(1); //command value size (bytes)
                        sb += assistant.toHex(command.currentIpOrPort);
                    } else {
                        warningMessage.push("Check coap ip/port selection parameter.");
                    }
                    break;
                case "11": /* Configure APN */
                    if (command.APN) {
                        sb += assistant.toHex(command.commandCode);
                        sb += assistant.toHex(33); //command value size (bytes)
                        sb += assistant.bytesToString(Buffer.from(command.APN.padEnd(33, '\0'), 'ascii'), 'hex').replace("-", "");
                    } else {
                        warningMessage.push("Check APN parameter.");
                    }
                    break;
                case "12": /* Configure Network */
                    if (typeof command.periodicTau === 'number' &&
                        typeof command.activeTime === 'number' &&
                        typeof command.pagingTime === 'number' &&
                        typeof command.eDRX === 'number') {
                        sb += assistant.toHex(command.commandCode);
                        sb += assistant.toHex(16); //command value size (bytes)
                        sb += assistant.bytesToString(bitconverter.getBytes(command.periodicTau), 'hex').replace("-", "");
                        sb += assistant.bytesToString(bitconverter.getBytes(command.activeTime), 'hex').replace("-", "");
                        sb += assistant.bytesToString(bitconverter.getBytes(command.pagingTime), 'hex').replace("-", "");
                        sb += assistant.bytesToString(bitconverter.getBytes(command.eDRX), 'hex').replace("-", "");
                    } else {
                        warningMessage.push("Check network parameters (periodic tau, active time, paging time, eDRX).");
                    }
                    break;
                case "13": /* Configure BackOff */
                    if (typeof command.backOffPeriod === 'number') {
                        sb += assistant.toHex(command.commandCode);
                        sb += assistant.toHex(4); //command value size (bytes)
                        sb += assistant.bytesToString(bitconverter.getBytes(command.backOffPeriod), 'hex').replace("-", "");
                    } else {
                        warningMessage.push("Check back-off period parameter.");
                    }
                    break;
                case "14": /* Reboot NBIOT module */
                    sb += assistant.toHex(command.commandCode);
                    sb += assistant.toHex(0); //command value size (bytes)
                    break;
                case "16": /* Change Encrypt/Decrypt Keys */
                    

                    if(command.changeEncryptKey && command.currentEncryptKey && command.newEncryptKey){ 
                        //[device encrypt key = server decrypt key]
                        sb += assistant.toHex(16);      //command code - 16
                        sb += assistant.toHex(35);       //command value size (bytes)
                        sb += assistant.toHex(2);      //[2 for encrypt]
                        sb += "1000";                 //key length = 16
                        sb += assistant.bytesToString(command.currentEncryptKey, 'hex');
                        sb += assistant.bytesToString(command.newEncryptKey, 'hex');
                    }

                    if(command.changeDecryptKey && command.currentDecryptKey && command.newDecryptKey){ 
                        //[device decrypt key = server encrypt key]
                        sb += assistant.toHex(16);      //command code - 16
                        sb += assistant.toHex(35);       //command value size (bytes)
                        sb += assistant.toHex(3);      //[3 for decrypt]
                        sb += "1000";                 //key length = 16
                        sb += assistant.bytesToString(command.currentDecryptKey, 'hex');
                        sb += assistant.bytesToString(command.newDecryptKey, 'hex');
                    }
                    
                    break;
                default:
                    break;
            }

        }
    }

    commandsInfo.output = sb.toUpperCase();
    commandsInfo.warnings = warningMessage;

    return commandsInfo;
};

//Reset Meter command packet
exports.createResetMeterCommand = function () {
    var sb = "";
    sb += assistant.toHex(1);       //command count (bytes)
    sb += assistant.toHex(1);       //command code - 01
    sb += assistant.toHex(0);       //command value size (bytes)

    return sb.toUpperCase();
};

//Transmit Interval configuration command packet
//transmitInterval(number) - Configure MIU with this new transmit interval value.
exports.createTransmitIntervalCommand = function (transmitInterval) {

    if (typeof transmitInterval !== 'number'){
        return "00";
    }

    var sb = "";
    sb += assistant.toHex(1);       //command count (bytes)
    sb += assistant.toHex(3);       //command code - 03
    sb += assistant.toHex(4);       //command value size (bytes)
    sb += assistant.changeHexOrder(transmitInterval.toString(16).padStart(8, '0'));

    return sb.toUpperCase();
};

//Log Interval configuration command packet
//logInterval (number) - Configure MIU with this new log interval value.
exports.createLogIntervalCommand = function (logInterval) {

    if (typeof logInterval !== 'number'){
        return "00";
    }

    var sb = "";
    sb += assistant.toHex(1);       //command count (bytes)
    sb += assistant.toHex(4);       //command code - 04
    sb += assistant.toHex(4);       //command value size (bytes)
    sb += assistant.changeHexOrder(logInterval.toString(16).padStart(8, '0'));

    return sb.toUpperCase();
};

//Clear Alarm configuration command packet
//clearAlarm (string) - Clear alarm based on bit mask. 0 = no action & 1 = clear alarm
//exp: clearAlarm = "01000011" means
// 0 - no action on leak alarm
// 1 - clear empty pipe alarm
// 0 - no action on burst pipe alarm 
// 0 - no action on tamper alarm 
// 0 - no action on low battery alarm 
// 0 - no action on backflow alarm 
// 1 - clear magnet tamper alarm 
// 1 - clear tilt alarm 
exports.createClearAlarmCommand = function (clearAlarm) {
    var message = clearAlarm.split('');
    var integer = parseInt(message.reverse().join(''), 2);

    var sb = "";
    var r = assistant.stringToBytes(clearAlarm);
    sb += assistant.toHex(1);       //command count (bytes)
    sb += assistant.toHex(5);       //command code - 05
    sb += assistant.toHex(1);       //command value size (bytes)
    sb += assistant.toHex(integer);

    return sb;
};

//Alarm Mask configuration command packet
//alarmMask (string) - Mask alarm based on bit mask.. 0 = no action & 1 = mask alarm
//exp: clearAlarm = "11100100" means
// 1 - mask leak alarm
// 1 - mask empty pipe alarm
// 1 - mask on burst pipe alarm 
// 0 - no action on tamper alarm 
// 0 - no action on low battery alarm 
// 1 - mask backflow alarm 
// 0 - mask magnet tamper alarm 
// 0 - mask tilt alarm 
exports.createMaskAlarmCommand = function (alarmMask) {
    var message = alarmMask.split('');
    var integer = parseInt(message.join(''), 2);

    var sb = "";
    sb += assistant.toHex(1);       //command count (bytes)
    sb += assistant.toHex(6);       //command code - 06
    sb += assistant.toHex(1);       //command value size (bytes)
    sb += assistant.toHex(integer);

    return sb;
};

//Low-Battery threshold configuration command packet
//batteryVoltage (float) - Threshold to flag low-battery alarm.
exports.createLowBatteryThresholdCommand = function (batteryVoltage) {

    if (typeof batteryVoltage !== 'number'){
        return "00";
    }

    var sb = "";
    sb += assistant.toHex(1);       //command count (bytes)
    sb += assistant.toHex(7);       //command code - 07
    sb += assistant.toHex(4);       //command value size (bytes)
    sb += assistant.bytesToString(bitconverter.doubleToBytes(batteryVoltage), 'hex').replace("-", "");

    return sb.toUpperCase();
};

//Pipe parameter configuration command packet
//burstThreshold (float) - Threshold to trigger Burst alarm. Value in m3
//burstSamplingInterval (int) - Sampling interval to check Burst threshold. Value in seconds.
//noConsumptionSamplingInterval (int) - Sampling interval to check No Consumption alarm. Value in seconds.
//leakageThreshold (float) - Threshold to trigger Leakage alarm. Value in seconds.
//leakageSamplingInterval (int)- Sampling interval; to check Leakage threshold. Value in liter.
//backflowThreshold (int) - Back flow pulse count to trigger Backflow alarm.
exports.createPipeParameterCommand = function (burstThreshold, burstSamplingInterval, noConsumptionSamplingInterval,
    leakageThreshold, leakageSamplingInterval, backflowThreshold) {

    var sb = "";
    sb += assistant.toHex(1);       //command count (bytes)
    sb += assistant.toHex(8);       //command code - 08
    sb += assistant.toHex(24);      //command value size (bytes)
    sb += assistant.bytesToString(bitconverter.doubleToBytes(burstThreshold * 1000 / 3600), 'hex').replace("-", ""); /*burst_threshold*/
    sb += assistant.bytesToString(bitconverter.getBytes(burstSamplingInterval), 'hex').replace("-", ""); /*burst_sampling_interval*/
    sb += assistant.bytesToString(bitconverter.getBytes(noConsumptionSamplingInterval), 'hex').replace("-", ""); /*no_consump_sample_interval*/
    sb += assistant.bytesToString(bitconverter.doubleToBytes(leakageThreshold / 3600), 'hex').replace("-", ""); /*leakage_threshold*/
    sb += assistant.bytesToString(bitconverter.getBytes(leakageSamplingInterval), 'hex').replace("-", ""); /*leakage_sample_interval*/
    sb += assistant.bytesToString(bitconverter.getBytes(backflowThreshold), 'hex').replace("-", ""); /*backflow_threshold*/

    return sb.toUpperCase();
};

//Coap IP/Port configuration command packet
// coapIp1 (string) - IP address 1. Null terminated string of format: xxx.xxx.xxx.xxx   
// coapPort1 (string) - Port 1. Null terminated string of format: xxxx
// coapIp2 (string) - IP address 2. Null terminated string of format: xxx.xxx.xxx.xxx   
// coapIp2 (string) - Port 2. Null terminated string of format: xxxx
exports.createCoapIpPortCommand = function (coapIp1, coapPort1, coapIp2, coapPort2) {
    if (!coapIp1) {
        coapIp1 = "";
    }

    if (!coapIp2) {
        coapIp2 = "";
    }

    var sb = "";
    sb += assistant.toHex(1);       //command count (bytes)
    sb += assistant.toHex(9);       //command code - 09
    sb += assistant.toHex(42);      //command value size (bytes)
    sb += assistant.bytesToString(Buffer.from(coapIp1.padEnd(16, '\0'), 'ascii'), 'hex').replace("-", "");
    sb += assistant.bytesToString(Buffer.from(coapPort1.padEnd(5, '\0'), 'ascii'), 'hex').replace("-", "");
    sb += assistant.bytesToString(Buffer.from(coapIp2.padEnd(16, '\0'), 'ascii'), 'hex').replace("-", "");
    sb += assistant.bytesToString(Buffer.from(coapPort2.padEnd(5, '\0'), 'ascii'), 'hex').replace("-", "");

    return sb.toUpperCase();
};

//Coap IP/Port selection command packet
// currentIpOrPort (int) - Next nbiot boot use IP/Port pair as specified. (value MUST be 1 or 2 only)
exports.createCoapSelectionCommand = function (currentIpOrPort) {
    if (!parseInt(currentIpOrPort)) {
        return "00";
    }
    if (currentIpOrPort < 1 || currentIpOrPort > 2) {
        return "00";
    }

    var sb = "";
    sb += assistant.toHex(1);       //command count (bytes)
    sb += assistant.toHex(10);      //command code - 10
    sb += assistant.toHex(1);       //command value size (bytes)
    sb += assistant.toHex(currentIpOrPort);

    return sb.toUpperCase();
};

//NBIOT APN configuration command packet
// APN (string) - APN String
exports.createAPNCommand = function (APN) {
    if (!APN) {
        return "00";
    }

    var sb = "";
    sb += assistant.toHex(1);       //command count (bytes)
    sb += assistant.toHex(11);      //command code - 11
    sb += assistant.toHex(33);      //command value size (bytes)
    sb += assistant.bytesToString(Buffer.from(APN.padEnd(33, '\0'), 'ascii'), 'hex').replace("-", "");

    return sb.toUpperCase();
};

//NBIOT Network negotiation configuration command packet
// periodicTau (int) - NbIoT Periodic TAU negotiation value in seconds.
// activeTime (int) - NbIoT Active Time negotiation value in seconds.
// pagingTime (int) - NbIoT Paging Time negotiation value in miliseconds.
// eDRX (int) - NbIoT eDRX negotiation value in miliseconds.
exports.createNetworkCommand = function (periodicTau, activeTime, pagingTime, eDRX) {
    var sb = "";
    sb += assistant.toHex(1);       //command count (bytes)
    sb += assistant.toHex(12);      //command code - 12
    sb += assistant.toHex(16);      //command value size (bytes)
    sb += assistant.bytesToString(bitconverter.getBytes(periodicTau), 'hex').replace("-", "");
    sb += assistant.bytesToString(bitconverter.getBytes(activeTime), 'hex').replace("-", "");
    sb += assistant.bytesToString(bitconverter.getBytes(pagingTime), 'hex').replace("-", "");
    sb += assistant.bytesToString(bitconverter.getBytes(eDRX), 'hex').replace("-", "");

    return sb.toUpperCase();
};

//NBIOT attach back-off configuration command packet
// backOffPeriod (int) - Back-off time before next attach sequence. Value in minutes.
exports.createBackoffCommand = function (backOffPeriod) {

    if (typeof backOffPeriod !== 'number'){
        return "00";
    }

    var sb = "";
    sb += assistant.toHex(1);        //command count (bytes)
    sb += assistant.toHex(13);      //command code - 13
    sb += assistant.toHex(4);       //command value size (bytes)
    sb += assistant.bytesToString(bitconverter.getBytes(backOffPeriod), 'hex').replace("-", "");

    return sb.toUpperCase();
};

//Reboot NBIOT module command packet
exports.createRebootModuleCommand = function () {
    var sb = "";
    sb += assistant.toHex(1);       //command count (bytes)
    sb += assistant.toHex(14);      //command code - 14
    sb += assistant.toHex(0);       //command value size (bytes)

    return sb.toUpperCase();
};

//NBIOT Decryption Key Update command packet (decrypt key device = encrypt key coap server)
//currentDecryptKey (buffer) - current MIU decryption key
//newDecryptKey (buffer) - new MIU decryption key
exports.createUpdateDecryptKeyCommand = function (currentDecryptKey, newDecryptKey) {

    if(!currentDecryptKey || !newDecryptKey){
        return "00";
    }

    var sb = "";
    sb += assistant.toHex(1);       //command count (bytes)
    sb += assistant.toHex(16);      //command code - 16
    sb += assistant.toHex(35);       //command value size (bytes)
    sb += assistant.toHex(3);      //[3 for decrypt]
    sb += "1000";                 //key length = 16
    sb += assistant.bytesToString(currentDecryptKey, 'hex');
    sb += assistant.bytesToString(newDecryptKey, 'hex');

    return sb.toUpperCase();
};

//NBIOT Encryption Key Update command packet (encryt key device = decrypt key coap server)
//currentEncryptKey (buffer) - current MIU encryption key
//newEncryptKey (buffer) - new MIU encryption key
exports.createUpdateEncryptKeyCommand = function (currentEncryptKey, newEncryptKey) {

    if(!currentDecryptKey || !newDecryptKey || !currentEncryptKey || !newEncryptKey){
        return "00";
    }

    var sb = "";
    sb += assistant.toHex(1);       //command count (bytes)
    sb += assistant.toHex(16);      //command code - 16
    sb += assistant.toHex(35);       //command value size (bytes) 
    sb += assistant.toHex(2);      //[2 for encrypt]
    sb += "1000";                   //key length = 16
    sb += assistant.bytesToString(currentEncryptKey, 'hex');
    sb += assistant.bytesToString(newEncryptKey, 'hex');

    return sb.toUpperCase();
};

//NBIOT Decryption & Encryption Keys Update command packet
//currentDecryptKey (buffer) - current decryption key
//newDecryptKey (buffer) - new decryption key
//currentEncryptKey (buffer) - current encryption key
//newEncryptKey (buffer) - new encryption key
exports.createUpdateDecryptandEncryptKeyCommand = function (currentDecryptKey, newDecryptKey, currentEncryptKey, newEncryptKey) {

    if(!currentEncryptKey || !newEncryptKey){
        return "00";
    }

    var sb = "";
    sb += assistant.toHex(2);                                   //command count (bytes) [it's 2 because we actually send 2 downlink commands]
    sb += assistant.toHex(16);                                  //command code - 16
    sb += assistant.toHex(35);                                  //command value size (bytes) 
    sb += assistant.toHex(2);                                   //[2 for encrypt]
    sb += "1000";                                               //key length = 16
    sb += assistant.bytesToString(currentEncryptKey, 'hex');
    sb += assistant.bytesToString(newEncryptKey, 'hex');
    sb += assistant.toHex(16);                                  //command code - 16
    sb += assistant.toHex(35);                                  //command value size (bytes)
    sb += assistant.toHex(3);                                   //[3 for decrypt]
    sb += "1000";                                               //key length = 16
    sb += assistant.bytesToString(currentDecryptKey, 'hex');
    sb += assistant.bytesToString(newDecryptKey, 'hex');

    return sb.toUpperCase();
};

//Reboot NBIOT module command packet
exports.createRebootModuleCommand = function () {
    var sb = "";
    sb += assistant.toHex(1);       //command count (bytes)
    sb += assistant.toHex(14);      //command code - 14
    sb += assistant.toHex(0);       //command value size (bytes)

    return sb.toUpperCase();
};

function sliceBuf(buf, start, end) {
    return buf.toString("ascii").slice(start,end);
}

