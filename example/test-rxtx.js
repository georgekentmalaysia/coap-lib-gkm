var main = require('../index.js');

//example key used to decrypt payload from device
var decryptKeyString = "9126c7b70ed46241b91c65a53f2fe307";
var decryptKey = Buffer.from(decryptKeyString, 'hex');

//example key used to encrypt payload to device 
var encryptKeyString = "9d03d7858e4066a03b25d62c061f3d48";
var encryptKey = Buffer.from(encryptKeyString, 'hex');

var commandInfo = getExample1Commands();

function getExample1Commands() {

    //to test multiple command sent
    var meterCommandTransaction = [];

    var m3 = {};
    m3.commandCode = "16"; //16: change MIU encryption key
    m3.changeEncryptKey = true; //flag to change encrypt Key (required)
    m3.currentEncryptKey = decryptKey; //[device encryption key = server decryption key]
    m3.newEncryptKey = Buffer.from("7a9c0afcc37a786f5511f5698bd4f606", "hex");
    meterCommandTransaction.push(m3);

    var m2 = {};
    m2.commandCode = "16"; //16: change MIU decryption key
    m2.changeDecryptKey = true; //flag to change decrypt Key (required)
    m2.currentDecryptKey = encryptKey; //[device decryption key = server encryption key]
    m2.newDecryptKey = Buffer.from("2e7db1029f229c1b40fb4c4fc9c3991d", "hex");
    meterCommandTransaction.push(m2);



    // var m0 = {};
    // m0.commandCode = "09"; //09: set ip/port
    // m0.coapIp1 = "192.168.0.133";
    // m0.coapPort1 = "5689";
    // m0.coapIp2 = "192.168.0.183";
    // m0.coapPort2 = "5689";
    // meterCommandTransaction.push(m0);

    // var m1 = {};
    // m1.commandCode = "10"; //10: set current ip/port selection
    // m1.currentIpOrPort = 2;
    // meterCommandTransaction.push(m1);



    var multicommands = main.createMultipleDownlinkCommands(meterCommandTransaction);
    return multicommands;
}

var command1 = main.createUpdateDecryptandEncryptKeyCommand(encryptKey, Buffer.from("2e7db1029f229c1b40fb4c4fc9c3991d", "hex"), decryptKey, Buffer.from("7a9c0afcc37a786f5511f5698bd4f606", "hex"));

console.log("Command Tx Payload: FF");
console.log("Command 1: " + commandInfo.output);
console.log("Command 2: " + command1);