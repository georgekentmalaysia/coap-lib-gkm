const code = {
    Succeed: 0x00,
    InvalidCheckSum: 0x01,
    EmptyPayload: 0x02,
    EmptyIMEI: 0x04,
    FatalError: 0xFF
}

module.exports = {
    getCode(status){
        switch(status){
            case "Succeed":
                return code.Succeed;
            case "InvalidCheckSum":
                return code.InvalidCheckSum;
            case "EmptyPayload":
                return code.EmptyPayload;
            case "EmptyIMEI":
                return code.EmptyIMEI;
            case "FatalError":
                return code.FatalError;
            default:
                return code.FatalError;
        }
    }
}
