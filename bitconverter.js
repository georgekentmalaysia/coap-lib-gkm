'use strict';

module.exports = {
    getBytes(src) {
        var b = Buffer.alloc(4);
        b[0] = src;
        b[1] = src >> 8
        b[2] = src >> 16
        b[3] = src >> 24
        return b
    },
    doubleToBytes(number) {
        var buffer = new ArrayBuffer(4);     //use size 4    
        var longNum = new Float32Array(buffer);  
    
        longNum[0] = number;
    
        return Buffer.from(buffer, 0 ,4);  
    },
    toByte: (src, startIndex) => {
        startIndex = startIndex || 0;

        return (src[startIndex] << 24) >> 24;
    },
    toShort: (src, startIndex) => {
        startIndex = startIndex || 0;

        return ((src[startIndex] << 8 | src[startIndex + 1]) << 16) >> 16;
    },
    toInt: (src, startIndex) => {
        startIndex = startIndex || 0;

        return src[startIndex] << 24
            | src[startIndex + 1] << 16
            | src[startIndex + 2] << 8
            | src[startIndex + 3];
    }
}